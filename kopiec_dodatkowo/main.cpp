#include<iostream>
#include <string>
#include<ctime>
#include<cstdlib>
#include "kopiec.hh"
#include "doubleList.hh"

#define ILOSC 100

using namespace std;

int dl_kolejki=0;

template<class Object>
void heapSort(doubleList<Object> &dane,const char komparator);  // sortowanie przez kopcowanie

int main(int argc, char *argv[]){
  doubleList<int> lista;
  srand(time(NULL));
  for(int i=0;i<ILOSC;i++)lista.wstawOstatni(rand()%100);
  lista.screen();
  cout<<endl;
  heapSort(lista,'<');
  lista.screen();
  cout<<endl;
  lista.clear();
  return 0;
}

template<class Object>
void heapSort(doubleList<Object> &dane,const char komparator){
  Kopiec<Object> kopiec;
  kopiec.Komparator(komparator);  // definiowane uporzadkowanie na kopcu
  while(!dane.isEmpty()){
    try{
      kopiec.dodaj(dane.usunPierwszy());  // dodawanie elementow do kopca
    }catch(TreeEmptyException){}
  }
  while(!kopiec.isEmpty()){
    try{
      dane.wstawOstatni(kopiec.usun());  // zdejmowanie elementow z kopca
    }catch(TreeEmptyException){}
  }
  kopiec.clear();
}
