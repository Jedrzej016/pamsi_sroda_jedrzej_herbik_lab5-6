#include"Tree.hh"

class KopiecEmptyException{};

template <class Object>
class Kopiec:public Tree<Object>{  // kopiec w oparciu o drzewo
private:
  int elementPokolenia;  // numeruje od 1 do ostatniego elementu w pokoleniu
  int pokolenie;  // numeryje pokojenia od 0
  Object komparator;  // określa czy kopiec ma byc malejacy czy rosnacy
  bool allIsNull(wezelDrzewa<Object> *wezel);
  int IsNull(wezelDrzewa<Object> *wezel);
  void clear(wezelDrzewa<Object> *wezel);
  int Potega(int x, int p);
  void sortuj(wezelDrzewa<Object> *wezel);
  void sortujRoot(wezelDrzewa<Object> *wezel);
public:
  Object& Komparator(const char a){if(a=='<')komparator=1;else komparator=-1;return komparator;}  // ustawia komparator
  Kopiec();
  bool clear();
  Object& dodaj(const Object& obj);
  Object& usunKolejno()throw(KopiecEmptyException);
  Object& usun()throw(KopiecEmptyException);
};

template <class Object>  // liczy potege liczny naturalnej
int Kopiec<Object>::Potega(int x, int p){
  if(!p)return 1;
  else return x*Potega(x,p-1);
}

template <class Object>  // wywolanie czyszczenia
bool Kopiec<Object>::clear(){
  clear(this->root);
  this->galaz=0;
  pokolenie=0;
  elementPokolenia=0;
  komparator=1;
  if((this->root!=NULL)||this->dl)return false;
  return true;
}

template <class Object>  // rekurencyjne czyszczenie
void Kopiec<Object>::clear(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++)clear(wezel->getSon(i));
    delete wezel;
    if((wezel==this->root)&&(this->dl==1))this->root=NULL;
    this->dl--;
  }
}

template <class Object>  // rekurencyjne sortowanie uzywane podczas dodawania
void Kopiec<Object>::sortuj(wezelDrzewa<Object> *wezel){
  Object tmp;
  if(wezel!=NULL){
    if(wezel->getParent()!=NULL){
      if((komparator*wezel->getZawartosc()) < (komparator*wezel->getParent()->getZawartosc())){  // jeżeli rodzic ma wieskza wartosc to zamienia
	tmp=wezel->getParent()->getZawartosc();
        wezel->getParent()->getZawartosc()=wezel->getZawartosc();
	wezel->getZawartosc()=tmp;
	sortuj(wezel->getParent());  // gdy wystapila zamiana to mogla sie popsuc struktura kopca wiec sprawdza reszte
      }
    }
  }
}

template <class Object>  // rekurencyjne sortowanie uzywane podczas usuwania
void Kopiec<Object>::sortujRoot(wezelDrzewa<Object> *wezel){
  Object tmp;
  if(wezel!=NULL){
    if(wezel->getSon(0)!=NULL){
      if((komparator*wezel->getZawartosc()) > (komparator*wezel->getSon(0)->getZawartosc())){  // jeżeli lewy syn ma wieskza wartosc to zamienia
	tmp=wezel->getSon(0)->getZawartosc();
        wezel->getSon(0)->getZawartosc()=wezel->getZawartosc();
	wezel->getZawartosc()=tmp;
	sortujRoot(wezel->getSon(0));  // gdy wystapila zamiana to mogla sie popsuc struktura kopca wiec sprawdza reszte
      }
    }
    if(wezel->getSon(1)!=NULL){
      if((komparator*wezel->getZawartosc()) > (komparator*wezel->getSon(1)->getZawartosc())){  // jeżeli prawy syn ma wieskza wartosc to zamienia
	tmp=wezel->getSon(1)->getZawartosc();
        wezel->getSon(1)->getZawartosc()=wezel->getZawartosc();
	wezel->getZawartosc()=tmp;
	sortujRoot(wezel->getSon(1));  // gdy wystapila zamiana to mogla sie popsuc struktura kopca wiec sprawdza reszte
      }
    }
  }
}

template <class Object>
Kopiec<Object>::Kopiec(){
  this->root=NULL;
  this->dl=0;
  pokolenie=0;
  elementPokolenia=0;
  this->galaz=0;
}

// tak wyglada numeracja elementow na kopcu
// 0                           1
// 1	       	      1                  2
// 2	          1       2        3           4
// 3	       	1   2   3   4   5     6     7     8
// 4	       1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16

template <class Object>
Object& Kopiec<Object>::dodaj(const Object& obj){
  wezelDrzewa<Object> *wezel=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  wezelDrzewa<Object> *tmp=this->root;
  int wartosc=0;
  int tmpPokolenie=0;
  if(elementPokolenia==Potega(2,pokolenie)){  // aktualizacja poziomu kopca
    pokolenie++;
    elementPokolenia=0;
    // elementPokolenia ma wartosc taka ile jest elementow w pokoleniu
    // Root jest pokoleniem 0
  }
  elementPokolenia++;
  this->dl++;
  wezel=new wezelDrzewa<Object>;  // alokacja pamieci
  wezel->setZawartosc(obj);
  this->zawartosc=wezel->getZawartosc();
  tmpPokolenie=0;
  while(tmp!=NULL){  // przechodzi w wskazane miejsce kopca
    tmpPokolenie++;
    tmp_back=tmp;
    wartosc=((elementPokolenia-1)/Potega(2,pokolenie-tmpPokolenie))%2;  // wyznacza droge do elementu
    tmp=tmp->getSon(wartosc);
  }
  wezel->setParent(tmp_back);  // wiaze element z wezlem poprzednim
  if(tmp_back==NULL){
    this->root=wezel;
  }else{
    tmp_back->getSon(wartosc)=wezel;
  }
  sortuj(wezel);  // naprawia zachwiana strukture kopca
  return this->zawartosc;
}

template <class Object>  // usuwa element
Object& Kopiec<Object>::usunKolejno()throw(KopiecEmptyException){
  int kierunek=0;
  wezelDrzewa<Object> *tmp=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  int tmpPokolenie=0;  
  this->galaz=0;
  while(this->root!=NULL){  // gdy istnieje tylko this->root
    if(allIsNull(this->root)){
      this->zawartosc=this->root->getZawartosc();
      delete(this->root);
      this->dl--;
      pokolenie=0;
      elementPokolenia=0;
      this->root=NULL;
      if(this->dl)throw KopiecEmptyException();  // gdy licznik sie nie zgadza z iloscia elementow
      return this->zawartosc;
    }
    tmp=this->root;
    while(!allIsNull(tmp)){  // gdy sa jacys synowie
      tmpPokolenie++;
      tmp_back=tmp;
      kierunek=((elementPokolenia-1)/Potega(2,pokolenie-tmpPokolenie))%2;  // wyznacza droge do elementu
      tmp=tmp->getSon(kierunek);  // przeskakuje na syna najbardziej po prawej
    }
    this->zawartosc=tmp->getZawartosc();
    delete(tmp);                          // zwolnienie pamieci
    elementPokolenia--;
    if(elementPokolenia==0){
      pokolenie--;
      elementPokolenia=Potega(2,pokolenie);
    }
    tmp_back->setSon(NULL,kierunek);      // czyszczenie syna
    this->dl--;
    if(this->dl)return this->zawartosc;
  }
  throw KopiecEmptyException();
}

template <class Object>  // usuwa element
Object& Kopiec<Object>::usun()throw(KopiecEmptyException){
  int kierunek=0;
  wezelDrzewa<Object> *tmp=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  int tmpPokolenie=0;  
  this->galaz=0;
  while(this->root!=NULL){  // gdy istnieje tylko this->root
    if(allIsNull(this->root)){
      this->zawartosc=this->root->getZawartosc();
      delete(this->root);
      this->dl--;
      pokolenie=0;
      elementPokolenia=0;
      this->root=NULL;
      if(this->dl)throw KopiecEmptyException();  // gdy licznik sie nie zgadza z iloscia elementow
      return this->zawartosc;
    }
    tmp=this->root;
    while(!allIsNull(tmp)){  // gdy sa jacys synowie
      tmpPokolenie++;
      tmp_back=tmp;
      kierunek=((elementPokolenia-1)/Potega(2,pokolenie-tmpPokolenie))%2;  // wyznacza droge do elementu
      tmp=tmp->getSon(kierunek);  // przeskakuje na syna najbardziej po prawej
    }
    this->zawartosc=tmp->getZawartosc();
    delete(tmp);                          // zwolnienie pamieci
    elementPokolenia--;
    if(elementPokolenia==0){
      pokolenie--;
      elementPokolenia=Potega(2,pokolenie);
    }
    tmp_back->setSon(NULL,kierunek);      // czyszczenie syna
    this->dl--;
    kierunek=this->root->getZawartosc();
    this->root->getZawartosc()=this->zawartosc;
    this->zawartosc=kierunek;
    sortujRoot(this->root);
    if(this->dl)return this->zawartosc;
  }
  throw KopiecEmptyException();
}

template <class Object>
bool Kopiec<Object>::allIsNull(wezelDrzewa<Object> *wezel){  // zwraca true jezeli wszyscy synowie sa NULL
  if(wezel!=NULL)for(int i=0;i<WEZLY;i++)if(wezel->getSon(i)!=NULL)return 0;
  return 1;
}

template <class Object>  // zwraca liczbe syna ktory nie jest NULL liczac od lewej do konca
int Kopiec<Object>::IsNull(wezelDrzewa<Object> *wezel){
  int zwrot=0;
  if(wezel!=NULL)for(int i=0;i<WEZLY;i++)if(wezel->getSon(i)!=NULL)zwrot=i;
  return zwrot;
}
