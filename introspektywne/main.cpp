#include <iostream>
#include <string>
#include<ctime>
#include<cstdlib>
#include <cmath>
#include "doubleList.hh"
#include "kopiec.hh"

using namespace std;

template <class Object>
bool quickSort(doubleList<Object> &dane,const char komparator); // sortowanie szybkie

template<class Object>
void heapSort(doubleList<Object> &dane,const char komparator);  // sortowanie przez kopcowanie

template<class Object>
void introSort(doubleList<Object> &dane,const char komparator);  // sortowanie introspektywne

template<class Object>
void mergeSort(doubleList<Object> &dane,const char komparator);  // sortowanie przez scalanie

template<class Object>
bool introSort(doubleList<Object> &dane,const char komparator,int &wywolanie,int rozmiar);  // rekurencyje wywolanie introspektywnego sortowania

template<class Object>
bool podziel(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2);  // dzieli liste na dwie

int dl_kolejki=0;

int main(int argc, char *argv[]){
  doubleList<int> lista1,lista2,lista3;
  srand(time(NULL));
  for(int i=0,b;i<50;i++){
    b=rand()%100;
    lista1.wstawOstatni(b);
    lista2.wstawOstatni(b);
    lista3.wstawOstatni(b);
  }
  quickSort(lista1,'<');
  quickSort(lista2,'<');
  quickSort(lista3,'<');
  for(int i=0,b;i<50;i++){
    b=rand()%100;
    lista1.wstawOstatni(b);
    lista2.wstawOstatni(b);
    lista3.wstawOstatni(b);
  }
  cout<<endl;
  lista1.screen();
  quickSort(lista1,'<');
  cout<<endl<<"Skonczono quicksort"<<endl;
  lista1.screen();
  cout<<endl<<endl;
  lista2.screen();
  mergeSort(lista2,'<');
  cout<<endl<<"Skonczono scalanie"<<endl;
  lista2.screen();
  cout<<endl<<endl;
  lista3.screen();
  introSort(lista3,'<');
  cout<<endl<<"Skonczono introspektywne"<<endl;
  lista3.screen();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  cout<<endl<<endl;
  return 0;
}

template <class Object>
bool quickSort(doubleList<Object> &dane,const char komparator){
  doubleList<Object> Mniejsze,Piwot,Wieksze;
  Object wartosc=0,piwot;
  Object komp=0;
  if(komparator=='<')komp=1;
  else komp=-1;
  try{
    piwot=dane.usunPierwszy();  // bierze piwot jako pierwszy element listy
  }catch(doubleListEmptyException){  // jezeli lista nie zawiera elemetow to wyrzuca blad
    return false;
  }
  Piwot.wstawOstatni(piwot);  // dodaje wziety piwot do listy piwotow
  while(!dane.isEmpty()){  // sprawdza pozostale elementy i dzieli na mniejsze rowne (piwoty) albo wieksze
    wartosc=dane.usunPierwszy();
    if((komp*wartosc)>(komp*piwot)){
      Wieksze.wstawOstatni(wartosc);
    }else if((komp*wartosc)<(komp*piwot)){
      Mniejsze.wstawOstatni(wartosc);
    }else{
      Piwot.wstawOstatni(wartosc);
    }
  }
  quickSort(Mniejsze,komparator);  // rekurencyjnie sortuje liste mniejszych elementow
  quickSort(Wieksze,komparator);  // rekurencyjnie sortuje liste wiekszych elementow
  dane.clear();
  if(Mniejsze.isEmpty()&&Piwot.isEmpty()&&Wieksze.isEmpty()){  // laczy juz posortowane listy mniejszych piwotow i wiekszych w zaleznosci od tego ktore istnieja 
    std::cout<<"Brak elementow do sortowania"<<std::endl;
  }else if(Mniejsze.isEmpty()&&Piwot.isEmpty()){
    dane.getNext()=Wieksze.getNext();
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Wieksze.getDl();
  }else if(Mniejsze.isEmpty()&&Wieksze.isEmpty()){
    dane.getNext()=Piwot.getNext();
    dane.getBack()=Piwot.getBack();
    dane.getDl()=Piwot.getDl();
  }else if(Piwot.isEmpty()&&Wieksze.isEmpty()){
    dane.getNext()=Mniejsze.getNext();
    dane.getBack()=Mniejsze.getBack();
    dane.getDl()=Mniejsze.getDl();  
  }else if(Mniejsze.isEmpty()){
    Wieksze.getNext()->getBack()=Piwot.getBack();
    Piwot.getBack()->getNext()=Wieksze.getNext();
    dane.getNext()=Piwot.getNext();
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Piwot.getDl()+Wieksze.getDl();
  }else if(Piwot.isEmpty()){
    Mniejsze.getBack()->getNext()=Wieksze.getNext();
    Wieksze.getNext()->getBack()=Mniejsze.getBack();
    dane.getNext()=Mniejsze.getNext();
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Mniejsze.getDl()+Wieksze.getDl();
  }else if(Wieksze.isEmpty()){
    Mniejsze.getBack()->getNext()=Piwot.getNext();
    Piwot.getNext()->getBack()=Mniejsze.getBack();
    dane.getNext()=Mniejsze.getNext();
    dane.getBack()=Piwot.getBack();
    dane.getDl()=Mniejsze.getDl()+Piwot.getDl();
  }else{
    Mniejsze.getBack()->getNext()=Piwot.getNext();  // do ostatniego elementu mniejszych jest przyczepiany pierwszy element piwotow
    Piwot.getNext()->getBack()=Mniejsze.getBack();
    Wieksze.getNext()->getBack()=Piwot.getBack();   // do ostatniego elementu piwotow jest przyczepiany pierwszy element wiekszych
    Piwot.getBack()->getNext()=Wieksze.getNext();
    dane.getNext()=Mniejsze.getNext();  // cala lista powstala z list mniejszej piwotow i wiekszej jest wstawiana do pustej listy dane
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Mniejsze.getDl()+Piwot.getDl()+Wieksze.getDl();  // danym jest przypisywana dlugosc rowna sumie trzech list
  }
  return true;
}

template<class Object>
void heapSort(doubleList<Object> &dane,const char komparator){
  Kopiec<Object> kopiec;
  kopiec.Komparator(komparator);  // definiowane uporzadkowanie na kopcu
  while(!dane.isEmpty()){
    try{
      kopiec.dodaj(dane.usunPierwszy());  // dodawanie elementow do kopca
    }catch(TreeEmptyException){}
  }
  while(!kopiec.isEmpty()){
    try{
      dane.wstawOstatni(kopiec.usun());  // zdejmowanie elementow z kopca
    }catch(TreeEmptyException){}
  }
  kopiec.clear();
}

template<class Object>
void mergeSort(doubleList<Object> &dane,const char komparator){
  doubleList<Object> lista1;
  doubleList<Object> lista2;
  if(podziel(dane,lista1,lista2)){  // jezeli podzial nastapil poprawnie to wywolaj sortowanie podlist
    mergeSort(lista1,komparator);
    mergeSort(lista2,komparator);
  }
  polacz(dane,lista1,lista2,komparator);  // laczenie posortowanych list
  lista1.clear();
  lista2.clear();
}

template<class Object>
void polacz(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2,const char komparator){
  Object komp=0;
  if(komparator=='<')komp=1;
  else komp=-1;
  while(!(list1.isEmpty()&&list2.isEmpty())){  // jezeli listy istnieja
    if(list1.isEmpty()){  // jezeli lista1 pusta to dodaj do danych cala liste2
      if(list.getDl()==0){  // jezeli dane sa puste
	list.getNext()=list2.getNext();  // wskazniki danych wskazuja teraz liste 2
	list.getBack()=list2.getBack();
	list.getDl()=list2.getDl();  // dlugosc jest rowna liscie 2
	list2.getNext()=NULL;  // lista 2 jest czyszczona
	list2.getBack()=NULL;
	list2.getDl()=0;
      }else{  // jezeli w danych juz sa elementy
	list.getBack()->getNext()=list2.getNext();  // lista 2 jest dodawana do konca listy danych
	list2.getNext()->setBack(list.getBack());
	list.getBack()=list2.getBack();
	list.getDl()=list.getDl()+list2.getDl();  // dane zwiekszana sie o dlugosc listy 2
	list2.getNext()=NULL;  // lista 2 jest czyszczona
	list2.getBack()=NULL;
	list2.getDl()=0;	  
      }
    }else if(list2.isEmpty()){  // jezeli lista2 pusta to dodaj do danych cala liste1
      if(list.getDl()==0){
	list.getNext()=list1.getNext();
	list.getBack()=list1.getBack();
	list.getDl()=list1.getDl();
	list1.getNext()=NULL;
	list1.getBack()=NULL;
	list1.getDl()=0;
      }else{
	list.getBack()->getNext()=list1.getNext();
	list1.getNext()->setBack(list.getBack());
	list.getBack()=list1.getBack();
	list.getDl()=list.getDl()+list1.getDl();
	list1.getNext()=NULL;
	list1.getBack()=NULL;
	list1.getDl()=0;	  
      }
    }else{  // jezeli obie listy istnieja to dodawaj z ich poczatkow ten element ktory jest mniejszy
      if((list1.pierwszy()*komp)<(list2.pierwszy()*komp))list.wstawOstatni(list1.usunPierwszy());
      else list.wstawOstatni(list2.usunPierwszy());
    }
  }
}

template<class Object>
bool podziel(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2){
  doubleListInside<Object> *tmp=list.getNext();    
  list1.clear();
  list2.clear();
  if(list.getDl()>1){
    for(int i=1;i<list.getDl()/2;i++){  // przeskakuje polowe elementow listy
      tmp=tmp->getNext();
    }
    list2.getNext()=tmp->getNext();  // dzieli dane na dwie listy a dane czysci
    list2.getBack()=list.getBack();
    list2.getDl()=list.getDl()-list.getDl()/2;
    tmp->getNext()->getBack()=NULL;
    tmp->getNext()=NULL;
    list1.getNext()=list.getNext();
    list1.getBack()=tmp;
    list1.getDl()=list.getDl()/2;
    list.getNext()=NULL;
    list.getBack()=NULL;
    list.getDl()=0;
    return true;
  }
  if(list.getDl()==1)list1.wstawOstatni(tmp->getZawartosc());  // jezeli dane maja tylko jeden element
  list.clear();
  return false;	  
}

template<class Object>
void introSort(doubleList<Object> &dane,const char komparator){  // wywolanie sortowania introspektywnego
  int wywolanie=0;
  int rozmiar=dane.rozmiar();
  introSort(dane,komparator,wywolanie,rozmiar);  // wywolanie rekurencyjne sortowania introspektywnego
}

template<class Object>
bool introSort(doubleList<Object> &dane,const char komparator,int &wywolanie,int dlugoscSortowania){
  doubleList<Object> Mniejsze,Piwot,Wieksze;
  Object wartosc=0,piwot;
  Object komp=0;
  wywolanie++;  // zmienna liczaca ilosc wywolan rekurencyjnych
  if((wywolanie>log(dlugoscSortowania))&&(dlugoscSortowania>1000)){  // jezeli ilosc wywolan przekracza ln(n to sortowanie tego wywolania jest sortowanie przez scalanie
    mergeSort(dane,komparator);
    wywolanie--;
    return false;
  }else{  // gdy wywolan jest mniej niz ln(n) to sortowanie jest sortowaniem szybkim
    if(komparator=='<')komp=1;
    else komp=-1;
    try{
      piwot=dane.usunPierwszy();
    }catch(doubleListEmptyException){
      wywolanie--;
      return false;
    }
    Piwot.wstawOstatni(piwot);
    while(!dane.isEmpty()){
      wartosc=dane.usunPierwszy();
      if((komp*wartosc)>(komp*piwot)){
	Wieksze.wstawOstatni(wartosc);
      }else if((komp*wartosc)<(komp*piwot)){
	Mniejsze.wstawOstatni(wartosc);
      }else{
	Piwot.wstawOstatni(wartosc);
      }
    }
    introSort(Mniejsze,komparator,wywolanie,komparator);
    introSort(Wieksze,komparator,wywolanie,komparator);
    dane.clear();
    if(Mniejsze.isEmpty()&&Piwot.isEmpty()&&Wieksze.isEmpty()){
      std::cout<<"Brak elementow do sortowania"<<std::endl;
    }else if(Mniejsze.isEmpty()&&Piwot.isEmpty()){
      dane.getNext()=Wieksze.getNext();
      dane.getBack()=Wieksze.getBack();
      dane.getDl()=Wieksze.getDl();
    }else if(Mniejsze.isEmpty()&&Wieksze.isEmpty()){
      dane.getNext()=Piwot.getNext();
      dane.getBack()=Piwot.getBack();
      dane.getDl()=Piwot.getDl();
    }else if(Piwot.isEmpty()&&Wieksze.isEmpty()){
      dane.getNext()=Mniejsze.getNext();
      dane.getBack()=Mniejsze.getBack();
      dane.getDl()=Mniejsze.getDl();  
    }else if(Mniejsze.isEmpty()){
      Wieksze.getNext()->getBack()=Piwot.getBack();
      Piwot.getBack()->getNext()=Wieksze.getNext();
      dane.getNext()=Piwot.getNext();
      dane.getBack()=Wieksze.getBack();
      dane.getDl()=Piwot.getDl()+Wieksze.getDl();
    }else if(Piwot.isEmpty()){
      Mniejsze.getBack()->getNext()=Wieksze.getNext();
      Wieksze.getNext()->getBack()=Mniejsze.getBack();
      dane.getNext()=Mniejsze.getNext();
      dane.getBack()=Wieksze.getBack();
      dane.getDl()=Mniejsze.getDl()+Wieksze.getDl();
    }else if(Wieksze.isEmpty()){
      Mniejsze.getBack()->getNext()=Piwot.getNext();
      Piwot.getNext()->getBack()=Mniejsze.getBack();
      dane.getNext()=Mniejsze.getNext();
      dane.getBack()=Piwot.getBack();
      dane.getDl()=Mniejsze.getDl()+Piwot.getDl();
    }else{
      Mniejsze.getBack()->getNext()=Piwot.getNext();
      Piwot.getNext()->getBack()=Mniejsze.getBack();
      Wieksze.getNext()->getBack()=Piwot.getBack();
      Piwot.getBack()->getNext()=Wieksze.getNext();
      dane.getNext()=Mniejsze.getNext();
      dane.getBack()=Wieksze.getBack();
      dane.getDl()=Mniejsze.getDl()+Piwot.getDl()+Wieksze.getDl();
    }
    wywolanie--;
    return true;
  }
}
