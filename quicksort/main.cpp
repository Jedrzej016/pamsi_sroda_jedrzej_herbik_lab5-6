#include <iostream>
#include <string>
#include<ctime>
#include<cstdlib>
#include "doubleList.hh"

using namespace std;

int dl_kolejki=0;

template <class Object>
bool quickSort(doubleList<Object> &dane,const char komparator);

int main(int argc, char *argv[]){
  doubleList<int> lista;
  srand(time(NULL)); // ustalenie punktu poczatkowego w danym monencie czasu procesora
  for(int i;i<100;++i)lista.wstawOstatni(rand()%100);
  //  for(int i;i<10000;++i)lista.wstawOstatni(i);
  lista.screen();cout<<endl<<endl;
  quickSort(lista,'<');
  lista.screen();cout<<endl;
  lista.clear();
  cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl;
  return 0;
}

template <class Object>
bool quickSort(doubleList<Object> &dane,const char komparator){
  doubleList<Object> Mniejsze,Piwot,Wieksze;
  Object wartosc=0,piwot;
  Object komp=0;
  if(komparator=='<')komp=1;
  else komp=-1;
  try{
    piwot=dane.usunPierwszy();  // bierze piwot jako pierwszy element listy
  }catch(doubleListEmptyException){  // jezeli lista nie zawiera elemetow to wyrzuca blad
    return false;
  }
  Piwot.wstawOstatni(piwot);  // dodaje wziety piwot do listy piwotow
  while(!dane.isEmpty()){  // sprawdza pozostale elementy i dzieli na mniejsze rowne (piwoty) albo wieksze
    wartosc=dane.usunPierwszy();
    if((komp*wartosc)>(komp*piwot)){
      Wieksze.wstawOstatni(wartosc);
    }else if((komp*wartosc)<(komp*piwot)){
      Mniejsze.wstawOstatni(wartosc);
    }else{
      Piwot.wstawOstatni(wartosc);
    }
  }
  quickSort(Mniejsze,komparator);  // rekurencyjnie sortuje liste mniejszych elementow
  quickSort(Wieksze,komparator);  // rekurencyjnie sortuje liste wiekszych elementow
  dane.clear();
  if(Mniejsze.isEmpty()&&Piwot.isEmpty()&&Wieksze.isEmpty()){  // laczy juz posortowane listy mniejszych piwotow i wiekszych w zaleznosci od tego ktore istnieja 
    std::cout<<"Brak elementow do sortowania"<<std::endl;
  }else if(Mniejsze.isEmpty()&&Piwot.isEmpty()){
    dane.getNext()=Wieksze.getNext();
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Wieksze.getDl();
  }else if(Mniejsze.isEmpty()&&Wieksze.isEmpty()){
    dane.getNext()=Piwot.getNext();
    dane.getBack()=Piwot.getBack();
    dane.getDl()=Piwot.getDl();
  }else if(Piwot.isEmpty()&&Wieksze.isEmpty()){
    dane.getNext()=Mniejsze.getNext();
    dane.getBack()=Mniejsze.getBack();
    dane.getDl()=Mniejsze.getDl();  
  }else if(Mniejsze.isEmpty()){
    Wieksze.getNext()->getBack()=Piwot.getBack();
    Piwot.getBack()->getNext()=Wieksze.getNext();
    dane.getNext()=Piwot.getNext();
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Piwot.getDl()+Wieksze.getDl();
  }else if(Piwot.isEmpty()){
    Mniejsze.getBack()->getNext()=Wieksze.getNext();
    Wieksze.getNext()->getBack()=Mniejsze.getBack();
    dane.getNext()=Mniejsze.getNext();
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Mniejsze.getDl()+Wieksze.getDl();
  }else if(Wieksze.isEmpty()){
    Mniejsze.getBack()->getNext()=Piwot.getNext();
    Piwot.getNext()->getBack()=Mniejsze.getBack();
    dane.getNext()=Mniejsze.getNext();
    dane.getBack()=Piwot.getBack();
    dane.getDl()=Mniejsze.getDl()+Piwot.getDl();
  }else{
    Mniejsze.getBack()->getNext()=Piwot.getNext();  // do ostatniego elementu mniejszych jest przyczepiany pierwszy element piwotow
    Piwot.getNext()->getBack()=Mniejsze.getBack();
    Wieksze.getNext()->getBack()=Piwot.getBack();   // do ostatniego elementu piwotow jest przyczepiany pierwszy element wiekszych
    Piwot.getBack()->getNext()=Wieksze.getNext();
    dane.getNext()=Mniejsze.getNext();  // cala lista powstala z list mniejszej piwotow i wiekszej jest wstawiana do pustej listy dane
    dane.getBack()=Wieksze.getBack();
    dane.getDl()=Mniejsze.getDl()+Piwot.getDl()+Wieksze.getDl();  // danym jest przypisywana dlugosc rowna sumie trzech list
  }
  return true;
}
