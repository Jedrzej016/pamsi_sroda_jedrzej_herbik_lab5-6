#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include "sortowania.hh"
#define ILOSC 5
#define POWTORZENIA 100
#define ZAKRES_LICZB 100

using namespace std;

int main(int argc, char *argv[]){
  ofstream plik_wyj("wyniki.txt");
  doubleList<int> tmp,lista1,lista2,lista3;
  double czas;
  clock_t begin;              // zmienna od poczatku pomiaru
  clock_t end;                // zmienna od konca pomiaru
  int ilosc[ILOSC]={10000,50000,100000,500000,1000000};
  int min=0;
  srand(time(NULL));
  plik_wyj<<endl<<"Pomiary czasow sortowan"<<endl<<endl;

  ///////////////////////////////////////////////////////////////////////////  lista losowa
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy losowej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){  // porownuje wszystkie elementy list 1, 2, 3 i zwraca blad jezeli sa rozne
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista 25% posortowana
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy w 25% posortowanej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){  // alokuje liste n elementowa w sposob losowy
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'<');          // sortuje cala liste
      quickSort(lista2,'<');
      quickSort(lista3,'<');
      for(int i=0;i<(75*ilosc[k])/100;i++){  // kasuje liste zostawiajac x% listy
	lista1.usunOstatni();
	lista2.usunOstatni();
	lista3.usunOstatni();
      }
      min=lista1.ostatni();  // sprawda jaka wartosc jest po x% listy
      for(int i=0,b;i<(75*ilosc[k])/100;i++){
	b=min+rand()%(ZAKRES_LICZB-min);
	lista1.wstawOstatni(b);  // z przedzialu od sprawdzonej wartosci do max alokuje na nowo liste w sposob losowy
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista 50% posortowana
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy w 50% posortowanej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'<');
      quickSort(lista2,'<');
      quickSort(lista3,'<');
      for(int i=0;i<(50*ilosc[k])/100;i++){
	lista1.usunOstatni();
	lista2.usunOstatni();
	lista3.usunOstatni();
      }
      min=lista1.ostatni();
      for(int i=0,b;i<(50*ilosc[k])/100;i++){
	b=min+rand()%(ZAKRES_LICZB-min);
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista 75% posortowana
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy w 75% posortowanej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'<');
      quickSort(lista2,'<');
      quickSort(lista3,'<');
      for(int i=0;i<(25*ilosc[k])/100;i++){
	lista1.usunOstatni();
	lista2.usunOstatni();
	lista3.usunOstatni();
      }
      min=lista1.ostatni();
      for(int i=0,b;i<(25*ilosc[k])/100;i++){
	b=min+rand()%(ZAKRES_LICZB-min);
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista 95% posortowana
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy w 95% posortowanej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'<');
      quickSort(lista2,'<');
      quickSort(lista3,'<');
      for(int i=0;i<(5*ilosc[k])/100;i++){
	lista1.usunOstatni();
	lista2.usunOstatni();
	lista3.usunOstatni();
      }
      min=lista1.ostatni();
      for(int i=0,b;i<(5*ilosc[k])/100;i++){
	b=min+rand()%(ZAKRES_LICZB-min);
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista 99% posortowana
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy w 99% posortowanej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'<');
      quickSort(lista2,'<');
      quickSort(lista3,'<');
      for(int i=0;i<(ilosc[k])/100;i++){
	lista1.usunOstatni();
	lista2.usunOstatni();
	lista3.usunOstatni();
      }
      min=lista1.ostatni();
      for(int i=0,b;i<(ilosc[k])/100;i++){
	b=min+rand()%(ZAKRES_LICZB-min);
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista 99,7% posortowana
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy w 99,7% posortowanej. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'<');
      quickSort(lista2,'<');
      quickSort(lista3,'<');
      for(int i=0;i<(3*ilosc[k])/1000;i++){
	lista1.usunOstatni();
	lista2.usunOstatni();
	lista3.usunOstatni();
      }
      min=lista1.ostatni();
      for(int i=0,b;i<(3*ilosc[k])/1000;i++){
	b=min+rand()%(ZAKRES_LICZB-min);
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  ///////////////////////////////////////////////////////////////////////////  lista posortowana od tylu
  for(int k=0;k<ILOSC;k++){
    plik_wyj<<"Sortowanie listy posortowanej od tylu. Ilosc elem: "<<ilosc[k]<<endl;
    for(int j=0,licznik=1;j<POWTORZENIA;j++){
      czas=0;
      for(int i=0,b;i<ilosc[k];i++){
	b=rand()%ZAKRES_LICZB;
	lista1.wstawOstatni(b);
	lista2.wstawOstatni(b);
	lista3.wstawOstatni(b);
      }
      quickSort(lista1,'>');
      quickSort(lista2,'>');
      quickSort(lista3,'>');
      begin=clock();  
      quickSort(lista1,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      if(licznik<10)plik_wyj<<"  "<<licznik++<<endl;
      else if(licznik<100)plik_wyj<<" "<<licznik++<<endl;
      else plik_wyj<<licznik++<<endl;
      plik_wyj<<"Czas quicksort:        "<<czas<<endl;
      begin=clock();  
      mergeSort(lista2,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas scalania:         "<<czas<<endl;
      begin=clock();  
      introSort(lista3,'<');
      end=clock();
      czas=double(end - begin)/CLOCKS_PER_SEC;
      plik_wyj<<"Czas introspektywnego: "<<czas<<endl;
      while(!lista1.isEmpty()){
	if(lista1.pierwszy()!=lista2.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.wstawOstatni(lista2.usunPierwszy());
	  lista1.usunPierwszy();
	}
      }
      while(!lista3.isEmpty()){
	if(tmp.pierwszy()!=lista3.pierwszy()){
	  tmp.clear();
	  lista1.clear();
	  lista2.clear();
	  lista3.clear();
	  cout<<"Niezgodnosc sortowan"<<endl;
	  return 1;
	}else{
	  tmp.usunPierwszy();
	  lista3.usunPierwszy();
	}
      }      
      tmp.clear();
      lista1.clear();
      lista2.clear();
      lista3.clear();
    }
  }
  tmp.clear();
  lista1.clear();
  lista2.clear();
  lista3.clear();
  
  plik_wyj.close(); // zamkniecie pliku
  cout<<"Test przebiegl pomyslnie."<<endl<<"Koniec pomiarow"<<endl<<endl;
  return 0;
}
