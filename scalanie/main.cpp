#include <iostream>
#include <string>
#include "doubleList.hh"

using namespace std;

int dl_kolejki=0;

template<class Object>
bool podziel(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2);  // dzieli liste na dwie

template<class Object>
void polacz(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2,const char komparator);  // laczy dwie listy w okreslony sposob (rosnoco lub malejaco)

template<class Object>
void mergeSort(doubleList<Object> &dane,const char komparator);  // sortowanie przez scalanie

int main(int argc, char *argv[]){
  doubleList<int> lista;
  for(int i=0;i<100;i++){
    lista.wstawOstatni(i%10);
  }
  lista.screen();
  cout<<endl;
  mergeSort(lista,'<');
  lista.screen();
  cout<<endl;
  lista.clear();
  return 0;
}
 
template<class Object>
void mergeSort(doubleList<Object> &dane,const char komparator){
  doubleList<Object> lista1;
  doubleList<Object> lista2;
  if(podziel(dane,lista1,lista2)){  // jezeli podzial nastapil poprawnie to wywolaj sortowanie podlist
    mergeSort(lista1,komparator);
    mergeSort(lista2,komparator);
  }
  polacz(dane,lista1,lista2,komparator);  // laczenie posortowanych list
  lista1.clear();
  lista2.clear();
}

template<class Object>
void polacz(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2,const char komparator){
  Object komp=0;
  if(komparator=='<')komp=1;
  else komp=-1;
  while(!(list1.isEmpty()&&list2.isEmpty())){  // jezeli listy istnieja
    if(list1.isEmpty()){  // jezeli lista1 pusta to dodaj do danych cala liste2
      if(list.getDl()==0){  // jezeli dane sa puste
	list.getNext()=list2.getNext();  // wskazniki danych wskazuja teraz liste 2
	list.getBack()=list2.getBack();
	list.getDl()=list2.getDl();  // dlugosc jest rowna liscie 2
	list2.getNext()=NULL;  // lista 2 jest czyszczona
	list2.getBack()=NULL;
	list2.getDl()=0;
      }else{  // jezeli w danych juz sa elementy
	list.getBack()->getNext()=list2.getNext();  // lista 2 jest dodawana do konca listy danych
	list2.getNext()->setBack(list.getBack());
	list.getBack()=list2.getBack();
	list.getDl()=list.getDl()+list2.getDl();  // dane zwiekszana sie o dlugosc listy 2
	list2.getNext()=NULL;  // lista 2 jest czyszczona
	list2.getBack()=NULL;
	list2.getDl()=0;	  
      }
    }else if(list2.isEmpty()){  // jezeli lista2 pusta to dodaj do danych cala liste1
      if(list.getDl()==0){
	list.getNext()=list1.getNext();
	list.getBack()=list1.getBack();
	list.getDl()=list1.getDl();
	list1.getNext()=NULL;
	list1.getBack()=NULL;
	list1.getDl()=0;
      }else{
	list.getBack()->getNext()=list1.getNext();
	list1.getNext()->setBack(list.getBack());
	list.getBack()=list1.getBack();
	list.getDl()=list.getDl()+list1.getDl();
	list1.getNext()=NULL;
	list1.getBack()=NULL;
	list1.getDl()=0;	  
      }
    }else{  // jezeli obie listy istnieja to dodawaj z ich poczatkow ten element ktory jest mniejszy
      if((list1.pierwszy()*komp)<(list2.pierwszy()*komp))list.wstawOstatni(list1.usunPierwszy());
      else list.wstawOstatni(list2.usunPierwszy());
    }
  }
}

template<class Object>
bool podziel(doubleList<Object> &list,doubleList<Object> &list1,doubleList<Object> &list2){
  doubleListInside<Object> *tmp=list.getNext();    
  list1.clear();
  list2.clear();
  if(list.getDl()>1){
    for(int i=1;i<list.getDl()/2;i++){  // przeskakuje polowe elementow listy
      tmp=tmp->getNext();
    }
    list2.getNext()=tmp->getNext();  // dzieli dane na dwie listy a dane czysci
    list2.getBack()=list.getBack();
    list2.getDl()=list.getDl()-list.getDl()/2;
    tmp->getNext()->getBack()=NULL;
    tmp->getNext()=NULL;
    list1.getNext()=list.getNext();
    list1.getBack()=tmp;
    list1.getDl()=list.getDl()/2;
    list.getNext()=NULL;
    list.getBack()=NULL;
    list.getDl()=0;
    return true;
  }
  if(list.getDl()==1)list1.wstawOstatni(tmp->getZawartosc());  // jezeli dane maja tylko jeden element
  list.clear();
  return false;	  
}
